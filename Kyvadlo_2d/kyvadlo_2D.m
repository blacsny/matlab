function kyvadlo_2D
%dvojraamen� kyvadlo
tic

% �asov� rozp�tie, v ktorom sa rie�ia diferenci�lne rovnice
tmax=30;
tspan1 = [0, tmax];
% za�iato�n� podmienky 
fi1=30/360*2*pi;
fi2=-30/360*2*pi;
w1=0;
w2=0;
x0 = [fi1; fi2; w1; w2]; % x je vektor r fi pr
% parametre diferenci�lnych rovn�c
% v0=20000;% r�chlos� v �ase 0
m1=5; %hmotnos� plan�ty (Zem)
m2=1; %hmotnos� druhej plan�ty (Slnko)
l1=0.5;
l2=1;

 %moment hybnosti
g=9.81; %gravita�n� kon�tanta

function dxdt = kyv(t,x,m1,m2,l1,l2,g)

    dxdt = [x(3);...
            x(4);...      
    -m2/m1*(x(1)-x(2))*(g/l1+x(3)^2+l2/l1*x(4)^2);... 
          (x(1)-x(2))*(m2/m1*(g/l2+l1/l2*x(3)^2+x(4)^2)+l1/l2*x(3)^2)-g/l2*x(2)];
end

% volanie diferenci�lnej rovnice: @(t,y) s� premenn�   
ode = @(t,x) kyv(t,x,m1,m2,l1,l2,g);

% rie�enie diferenci�lnych rovn�c
options=odeset('RelTol',1e-4);
% InitialStep ur�uje horn� hranicu d�ky prv�ho kroku pri numerickom
% rie�en� DR. Ak sa neuvedie, ur�� sa automaticky zo strmosti rie�enia 
% v za�iato�nom �ase
% Refine je zv��enie po�tu bodov vykreslen�ch z krivky. ��slo hovor�, ko�ko
% bodov dopln� medzi p�vodn�mi dvomi susedn�mi bodmi
% RelTol je ve�kos� relat�vnej chyby, ktor� uspokoj� rie�ite�a. Zni�ovanie
% tejto hodnoty zvy�uje presnos��t rie�enia

[t,x] = ode45(ode, tspan1, x0, options);

% dvojrozmern� graf rie�enia 
%plot(x(:,1).*cos(x(:,2)),x(:,1).*sin(x(:,2)))
title('dve planety')
% pol�rny graf
% plot(l1*sin(x(:,1)),-l1*cos(x(:,1)))
% hold on
% plot(l1*sin(x(:,1))+l2*sin(x(:,2)),-(l1*cos(x(:,1))+l2*cos(x(:,2))),'r')
% polar(x(:,2),x(:,1))
[k,l]=size(x);
for i=1:k
x1=l1*sin(x(i,1));
x2=l1*sin(x(i,1))+l2*sin(x(i,2));
y1=-l1*cos(x(i,1));
y2=-(l1*cos(x(i,1))+l2*cos(x(i,2)));
data=[0 x1 x2 ; 0 y1 y2];
plot(data(1,:),data(2,:),'-o');
axis([-(l1+l2) (l1+l2) -(l1+l2) 0])
grid on
pause(.01)
end
toc
end

