function planet_2D
%Lorenzov atraktor
tic

% �asov� rozp�tie, v ktorom sa rie�ia diferenci�lne rovnice
tmax=86400*365*2;
tspan1 = [0, tmax];
% za�iato�n� podmienky 
r0=1.5e11;
fi0=0;
pr0=0;
x0 = [r0; fi0; pr0]; % x je vektor r fi pr
% parametre diferenci�lnych rovn�c
v0=20000;% r�chlos� v �ase 0
m1=5.94e24; %hmotnos� plan�ty (Zem)
m2=2e30; %hmotnos� druhej plan�ty (Slnko)
M=m1+m2;
mu=m1*m2/M;
J=mu*v0*r0; %moment hybnosti
G=6.67e-11; %gravita�n� kon�tanta

function dxdt = odrPlanet(t,x,mu,M,J,G)
dxdt = [x(3)/mu;...
          J/(mu*x(1)^2);... 
          J^2/(mu*x(1)^3)-G*mu*M/x(1)^2];
end

% volanie diferenci�lnej rovnice: @(t,y) s� premenn�   
ode = @(t,x) odrPlanet(t,x,mu,M,J,G);

% rie�enie diferenci�lnych rovn�c
options=odeset('Refine',10,'RelTol',1e-3);
% InitialStep ur�uje horn� hranicu d�ky prv�ho kroku pri numerickom
% rie�en� DR. Ak sa neuvedie, ur�� sa automaticky zo strmosti rie�enia 
% v za�iato�nom �ase
% Refine je zv��enie po�tu bodov vykreslen�ch z krivky. ��slo hovor�, ko�ko
% bodov dopln� medzi p�vodn�mi dvomi susedn�mi bodmi
% RelTol je ve�kos� relat�vnej chyby, ktor� uspokoj� rie�ite�a. Zni�ovanie
% tejto hodnoty zvy�uje presnos��t rie�enia

[t,x] = ode23(ode, tspan1, x0, options);

% dvojrozmern� graf rie�enia 
%plot(x(:,1).*cos(x(:,2)),x(:,1).*sin(x(:,2)))
title('dve planety')
% pol�rny graf
polar(x(:,2),x(:,1))
toc
end

